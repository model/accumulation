setHeadwaters <- function(nUpstream) {
  headwaterCells <- findheadwaters(nUpstream)
  countheadwaters(headwaterCells)
}

findheadwaters<-function(nUpstream) {
  nCells <- domainInfo$nActiveCells
  headwaterCells <- array(NA, dim = nCells)
  
  for (iCell in 1:nCells) {

    ## Check if there is an upstream cell!!!
    if (nUpstream[iCell] <= 0) {
      headwaterCells[iCell] <- 1
    }
  }
  
  nHeads<-length(headwaterCells[!is.na(headwaterCells)])
  cat(paste0("number of headwaters: ", nHeads, "\n\n"))
  ######## End find headwaters
  
  return(headwaterCells)
}

countheadwaters <- function(headwaterCells) {
  iHead <- 0
  for (iCell in 1:domainInfo$nActiveCells) {
    if (!is.na(headwaterCells[iCell])) {
      iHead <- iHead + 1
      headwaterCells[iCell] <- iHead
    }
  }
  return(headwaterCells)
}

