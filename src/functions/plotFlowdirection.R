plotFlowdirection<-function(flowdir, domainInfo) {
  
  vecFlowDir <- data.frame(lat=as.vector(domainInfo$vectActiveCells["lat"]), lon=as.vector(domainInfo$vectActiveCells["lon"]), direction=as.vector(flowdir))
  
  vecDestCell<-vecFlowDir
  for (i in 1:length(vecFlowDir[,1])) {
    vecDestCell[i,"lon"]<-vecFlowDir[i,"lon"] + ( lonDirection(vecFlowDir[i,"direction"]) * domainInfo$resolution)
    vecDestCell[i,"lat"]<-vecFlowDir[i,"lat"] + ( latDirection(vecFlowDir[i,"direction"]) * domainInfo$resolution)
  }
  
  plot(vecFlowDir[,"lon"], vecFlowDir[,"lat"], main = "flowdirection", xlab = "longitude", ylab = "latitude")
  segments( vecFlowDir[,"lon"], vecFlowDir[,"lat"], vecDestCell[,"lon"], vecDestCell[,"lat"], col='red' )
  
  ## iCellNetw = the index of the cell in the node netWorklist
  ## cellId = the unique ID of the gridcell
  for (iCellNetw in 1:nCellNetwork) {
    cellId <- network$indexOfActiveCells[iCellNetw]
    nodeId <- network$nodeId[iCellNetw]
    
    if (isHeadwater(iCellNetw) && isOutlet(iCellNetw)) {
      points(domainInfo$vectActiveCells[cellId,"lon"], domainInfo$vectActiveCells[cellId,"lat"], pch = 19, col = "pink", cex = 2)
      text(domainInfo$vectActiveCells[cellId,"lon"], domainInfo$vectActiveCells[cellId,"lat"], paste0(nodeId), col = "white", cex = .7)
    } else if (isHeadwater(iCellNetw)) {
      points(domainInfo$vectActiveCells[cellId,"lon"], domainInfo$vectActiveCells[cellId,"lat"], pch = 19, col = "red", cex = 2)
      text(domainInfo$vectActiveCells[cellId,"lon"], domainInfo$vectActiveCells[cellId,"lat"], paste0(nodeId), col = "white", cex = .7)
    } else if (isOutlet(iCellNetw)) {
      points(domainInfo$vectActiveCells[cellId,"lon"], domainInfo$vectActiveCells[cellId,"lat"], pch = 19, col = "green", cex = 2)
    } else {
      points(domainInfo$vectActiveCells[cellId,"lon"], domainInfo$vectActiveCells[cellId,"lat"], pch = 19, col = "grey", cex = domainInfo$vectActiveCells$nUpstream[cellId])
    }
  }
  
  for (cellId in 1:length(accumulation)) {
    # points(domainInfo$vectActiveCells[cellId,"lon"], domainInfo$vectActiveCells[cellId,"lat"], pch = 19, col = "grey", cex = accumulation[cellId]/10)
    text(domainInfo$vectActiveCells[cellId,"lon"]-.1, domainInfo$vectActiveCells[cellId,"lat"]-.2, accumulation[cellId], col = "black", cex = .7)
    text(domainInfo$vectActiveCells[cellId,"lon"]+.1, domainInfo$vectActiveCells[cellId,"lat"]-.2, cellId, col = "blue", cex = .7)
  }
  
  for (cellId in 1:length(accumulation)) {
    # points(domainInfo$vectActiveCells[cellId,"lon"], domainInfo$vectActiveCells[cellId,"lat"], pch = 19, col = "grey", cex = accumulation[cellId]/10)
#    text(domainInfo$vectActiveCells[cellId,"lon"], domainInfo$vectActiveCells[cellId,"lat"]-.2, accumulation[cellId], col = "black", cex = .7)
  }
  
}

