# Flow accumulation

## Install/Preperation

There is no need to install the script. Just clone it and run.
However, you need to have some dependencies installed on your system:

The bash script needs:
* cdo
* nco
* gdal_translate

The R script needs the package:
* ncd4

## How to run
To do a full run including preprocessing just execute
``` bash
./run_accumulation.sh
```

In case you only want to run the accumulation script (R-script) from bash:
``` bash
Rscript ./src/accumulate.R <flowdir.nc> <meanQFile.nc> <Cl_loadingFile.nc> <Cl_backgroundCFile.nc> <outFile.nc>
```
or directly from R:
``` R
source("./src/accumulate.R")
```
in the last case you need to adjust the following lines in ```./src/accumulate.R```
* https://git.wageningenur.nl/model/accumulation/blob/input_data/src/accumulate.R#L14-20



# License/Author

* Author: Wietse Franssen
* Email: wietse.franssen@wur.nl
